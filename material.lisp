(in-package :lrp)

(defclass material-manager ()
  ((materials :accessor materials
              :initform (make-hash-table))))

(defvar *material-manager* (make-instance 'material-manager))

(defgeneric material (object))

(defmethod material ((name symbol))
  (gethash name (materials *material-manager*)))

(defun (setf material) (value name)
  (setf (gethash name (materials *material-manager*)) value))

;; Standard materials

(setf (material :air) '(:solid nil))

(setf (material :floor) '(:solid t))

(setf (material :wall) '(:solid t))
